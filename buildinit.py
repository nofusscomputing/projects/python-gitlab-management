#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import sys, git
import os.path
from datetime import datetime

CurrentRepo = None
GIT_COMMIT = ''
GIT_BRANCH = ''

__author__ = "No Fuss Computing"
__email__ = "helpdesk@nofusscomputing.com"
__license__ = "GNU LGPLv3"
__copyright__ = "(C) All Rights reserved"
__source__ = ''
__title__ = "gitlab-management"
__version__ = "0.2.1"
__doc__ = "https://gitlab.com/nofusscomputing/projects/python-gitlab-management"

if '/docs' in os.environ['PWD']:
    CurrentDirectory = '../'
else:
    CurrentDirectory = './'

DistributionDirectory = 'dist'
ModuleDirectory = CurrentDirectory + 'gitlab_management/'
VersionFile = CurrentDirectory + DistributionDirectory + '/version'

OutputPrefix = '[BuildInitScript] '

def init() -> bool:
    init = True
    global __source__, __version__, CurrentRepo, GIT_BRANCH, GIT_COMMIT

    
    if os.path.isdir(CurrentDirectory + '.git'):

        CurrentRepo = git.Repo(search_parent_directories=True)
        GIT_COMMIT = CurrentRepo.head.object.hexsha
        
        # Check if running within a GitLab Runner
        if os.environ.get('CI_COMMIT_BRANCH') is None and os.environ.get('CI_JOB_ID') is None and os.environ.get('READTHEDOCS') is None:
            GIT_BRANCH = CurrentRepo.head.reference.name
        elif os.environ.get('READTHEDOCS') is not None:
            GIT_BRANCH = 'READTHEDOCS'
        else:
            GIT_BRANCH=os.environ.get('CI_COMMIT_BRANCH')

        print(OutputPrefix + 'Git Commit: ' + GIT_COMMIT)
        print(OutputPrefix + 'Git Branch: ' + GIT_BRANCH)

        __source__ = 'https://gitlab.com/nofusscomputing/projects/python-gitlab-management/-/tree/' + GIT_COMMIT

        if GIT_BRANCH != 'master':
            __version__ = __version__ + 'rc' + datetime.utcnow().strftime('%y%m%d%H%M%S')
            

    if not os.path.isdir(CurrentDirectory + DistributionDirectory):

        os.makedirs(CurrentDirectory + DistributionDirectory)

        if not os.path.isdir(CurrentDirectory + DistributionDirectory):
            print(OutputPrefix + 'couldnt Make Directory:' + CurrentDirectory + DistributionDirectory)
            init = False
        
    if not WriteFile(VersionFile, __version__):
        print(OutputPrefix + 'Could not create version file')
        init = False

    print(OutputPrefix + 'init success')

    return init


def BuildModuleInit() -> str:
    BuildModuleInit = None

    BuildModuleInit='''
#-*- coding: utf-8 -*-
"""
Todo
----
insert module comments in this space.

"""

__title__ = "''' + __title__ + '''"
__version__ = "''' + __version__ + '''"
__doc__ = "''' + __doc__ + '''"
__author__ = "''' + __author__ + '''"
__email__ = "''' + __email__ + '''"
__license__ = "''' + __license__ + '''"
__copyright__ = "''' + __copyright__ + '''"
__source__ = "''' + __source__ + '''"
'''

    print(OutputPrefix + "__init__.py generated contents:\n" + BuildModuleInit)

    return BuildModuleInit


def BuildDockerfile() -> str:
    BuildDockerfile = None
    
    BuildDockerfile = '''
FROM python:3.6.10-alpine3.12

LABEL \\
    maintainer="''' + __author__ + ''' <http://nofusscomputing.com> + " \\
    version="''' + __version__ + '''" \\
    licence="''' + __license__ + '''" \\
    source="''' + __source__ + '''" \\
    commit="''' + GIT_COMMIT + '''"

ADD dist/ /tmp/

RUN pip --version \\ 
    && pip install \\ 
        /tmp/$(ls tmp/ | grep -e ^gitlab_management.*\.whl) \\
    && gitlab-management -h

ENTRYPOINT [ "gitlab-management" ]

CMD [ "-h" ]'''


    print (OutputPrefix + "dockerfile generated contents:\n" + BuildDockerfile)

    return BuildDockerfile

def WriteFile(FileName:str, FileContent:str) -> bool:
    WriteFile = False

    with open(FileName, "w") as text_file:
        text_file.write(FileContent)
    
    if os.path.isfile(FileName):
        WriteFile = True
    else:
        print(OutputPrefix + 'Faile to write file: ' + FileName)

    return WriteFile


print(OutputPrefix + 'Starting init python script')

if init():
    
    if not WriteFile(CurrentDirectory + 'dockerfile', BuildDockerfile()):
        #sys.exit(753)
        pass

    if not WriteFile(ModuleDirectory + '__init__.py', BuildModuleInit()):
        #sys.exit(754)
        pass

else:
    print(OutputPrefix + 'Init Failure')
    sys.exit(755)


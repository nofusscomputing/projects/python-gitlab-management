# Python Gitlab Management

[![PyPi Version](https://img.shields.io/badge/dynamic/json?label=PyPi%20Package&query=$.info.version&url=https://pypi.org/pypi/gitlab-management/json&logo=pypi&logoColor=white&style=plastic)](https://pypi.org/project/gitlab-management/)
[![License: LGPL v3](https://img.shields.io/badge/License-LGPL%20v3-green.svg?style=plastic&logo=gnu&logocolor=white)](https://gitlab.com/nofusscomputing/projects/python-gitlab-management/-/blob/master/LICENCE)




[![Pipelien Status - Stable](https://img.shields.io/badge/dynamic/json.svg?label=Pipeline%20%5Bstable%5D&query=0.status&url=https://gitlab.com/api/v4/projects/19099644/pipelines?ref=master&color=ff782e&logo=gitlab&style=plastic)](https://gitlab.com/nofusscomputing/projects/python-gitlab-management/) 
[![Code Coverage - Stable](https://img.shields.io/badge/dynamic/json?color=F01F7A&label=Coverage%20%5Bstable%5D&query=%24.totals.percent_covered&suffix=%&logo=codecov&style=plastic&url=https%3A%2F%2Fnofusscomputing.gitlab.io%2Fprojects%2Fpython-gitlab-management%2Fmaster%2Fcoverage.json)](https://nofusscomputing.gitlab.io/projects/python-gitlab-management/master/coverage/) 
[![PyLint Score Master](https://img.shields.io/badge/dynamic/json?color=73398D&label=PyLint%20Score%20%5Bstable%5D&query=%24.PyLintScore&style=plastic&url=https:%2F%2Fgitlab.com%2Fnofusscomputing%2Fprojects%2Fpython-gitlab-management%2F-%2Fjobs%2Fartifacts%2Fmaster%2Ffile%2Fbadge_pylint.json?job=PyLint)](https://gitlab.com/nofusscomputing/projects/python-gitlab-management/-/jobs/artifacts/master/file/gl-code-quality-report.html?job=PyLint)
[![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/nofusscomputing/gitlab-management/stable?color=2496ED&label=Docker%20%5Bstable%5D&logo=docker&style=plastic)](https://hub.docker.com/r/nofusscomputing/gitlab-management)
[![Read the Docs (version)](https://img.shields.io/readthedocs/python-gitlab-management/stable?label=Docs%20stable&logo=readthedocs&style=plastic)](https://python-gitlab-management.readthedocs.io/en/stable/)


[![Pipelien Status - Dev](https://img.shields.io/badge/dynamic/json.svg?label=Pipeline%20%5BDev%5D&query=0.status&url=https://gitlab.com/api/v4/projects/19099644/pipelines/?ref=development&color=ff782e&logo=gitlab&style=plastic)](https://gitlab.com/nofusscomputing/projects/python-gitlab-management/)
[![Code Coverage - dev](https://img.shields.io/badge/dynamic/json?color=F01F7A&label=Coverage%20%5BDev%5D&query=%24.totals.percent_covered&suffix=%&logo=codecov&style=plastic&url=https://gitlab.com/nofusscomputing/projects/python-gitlab-management/-/jobs/artifacts/development/file/badge_coverage.json?job=Coverage)](https://nofusscomputing.gitlab.io/projects/python-gitlab-management/development/coverage/)
[![PyLint Score Dev](https://img.shields.io/badge/dynamic/json?color=73398D&label=PyLint%20Score%20%5BDev%5D&query=%24.PyLintScore&style=plastic&url=https://gitlab.com/nofusscomputing/projects/python-gitlab-management/-/jobs/artifacts/development/file/badge_pylint.json?job=PyLint)](https://gitlab.com/nofusscomputing/projects/python-gitlab-management/-/jobs/artifacts/development/file/gl-code-quality-report.html?job=PyLint)
[![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/nofusscomputing/gitlab-management/dev?color=2496ED&label=Docker%20%5Bdev%5D&logo=docker&style=plastic)](https://hub.docker.com/r/nofusscomputing/gitlab-management)
[![Read the Docs (version)](https://img.shields.io/readthedocs/python-gitlab-management/development?label=Docs%20devel&logo=readthedocs&style=plastic)](https://python-gitlab-management.readthedocs.io/en/development/)


Gitlab-management is python module that enables GitLab group configuration from code. By design it's intended to be setup to run on a schedule. 

## How to Use
GitLab-Management can be used from the command line or imported as a python module.

Feature wise this module only process labels (create/add). over time features will be added. please see the milestones, issue and merge requests at the home page.

For please refer to the docs.


### Python Module
To install run `pip install gitlab-management`. from there you will be able to use and extend the module as you see fit.

### CLI
Gitlab-management can be used via cli with `gitlab-management`, there is no requirement to prefix with `python3 -m`, as the command is registered on install. To view all available options, use switch `-h` or `--help`

### Config File
The configuration file for this module (`config.yml`) is a yml formated file that is required to be in the directory that the command is ran from.

The layout of the yml file
``` yaml
Group:
    Labels:
        -
            Group: Example1
            Name: Bug
            Description: "Items that are bugs or bug related"
            Color: "#FF0000"

        -
            Group: 
                - Example1
                - Example2
            Name: Feature
            Description: "Items that are feature related"
            Color: "#00FF00"

```

`Group.Labels` is a `list` of `dict` for each label that is to be created add a new `dict` to the `list` under `Group.Labels`

`Group.Labels.#.Group` can be a single string which is the name of an existing group that the user has access to as maintainer. `Group.Labels.#.Group` can also be a `list` of group names that the label will be added to.


### Docker container
further help can be found in the [documentation](https://python-gitlab-management.readthedocs.io/en/development/pages/docker.html).

The docker images have been desigend to run the module directly as a command, where you only have to specify the `CLI` arguments.

images are tagged for ease of idententification in accordance with the table below:

| Tag                |   repo branch   |  When created |
|--------------------|:---------------:|:------:|
| `{Version number}` |  Git Tag        | every git tag will create it's version |
| `dev`              |   development   |   when a merge to development branch occurs or a git tag is created on a branch not master. |
| `stable`           |     master      |    when a merge to master branch occurs |

## Issues, Feature Requests and Bugs
If an issue or bug is found within the package (i.e. exception), please [create an issue ticket](https://gitlab.com/nofusscomputing/projects/python-gitlab-management/-/issues) using the applicable issue template available at the time of ticket creation. If you would like to request a feature and are unable to contribute, [please create an issue](https://gitlab.com/nofusscomputing/projects/python-gitlab-management/-/issues) using the feature issue template.

## Contributing
Contribution guide can be viewed in the [repo.](https://gitlab.com/nofusscomputing/projects/python-gitlab-management/-/blob/master/CONTRIBUTING.md)


## Licence
The package licence can be viewed in the [repo](https://gitlab.com/nofusscomputing/projects/python-gitlab-management/-/blob/master/LICENSE)
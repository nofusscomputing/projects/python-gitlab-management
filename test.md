## Problem to solve

<!-- What problem do we solve? Try to define the who/what/why of the opportunity as a user story. For example, "As a (who), I want (what), so I can (why/value)." ) -->

Actions taken by the module should have the option to be logged for auditing purposes. This would allow actions performed to be tracked, checked and have metrics available should it be needed.


## Details
1. **project changed** if a user notices that something has changed in the project/group, an audit log would enable a check to confirm the what and when or if it was made by `gitlab-management`

    * [ ] ~Feature log to a specified location

1. **Usage tracking** with logging, you will be able to determine how much `gitlab-management` is being used. for this to occur the log file should be able to stand up to scrutiny.
    * [ ] ~Feature commit log to git repository
        * if the specified log location is a git repository, commit the log to the repository.
        * possible log message: `Logfile {filename} updated with {SHA1_HASH} SHA1 fingerprint.`
    * [ ] ~Feature Determine if log path `git sub-module`, commit to it, not parent repo.

1. **Specify log level** Minimum logging level will be `Information`, with the option to specify `Debug`. No other level will be available at this stage.

    * [ ] ~Feature Specify log level 

1. **Log format** the log file will be plain text. at this stage, it will be formatted as a csv. A CSV will allow easy import into many different programs for further review or madification.

    * [ ] ~Feature Log file a CSV.



<!-- Include use cases, benefits, goals, or any other details that will help us understand the problem better. -->

## Documentation

* [ ] ~Documentation CLI help
    * There needs to be a Switch for activating this feature.
* [ ] ~Documentation page for python-gitlab-management.readthedocs.io
<!-- 
Add all known Documentation Requirements in this section. 
 -->

 ### Links / references

 <!-- place any links here including any references. i.e. any other python modules required-->

 /label ~Feature ~"Documentation::Not Started" 
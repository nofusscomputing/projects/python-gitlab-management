#/bin/bash



function Run_Commands {

    $@;

    ExitCode=$?

    if ! [ $ExitCode = 0 ]; then
        echo "error with command:[$@], exit status ($ExitCode)"
        exit $ExitCode
    fi

}

Run_Commands "echo Project Directory: $CI_PROJECT_DIR"

# These are a manual install as they don't install from the requirements file
Run_Commands "pip3 install --no-cache-dir m2r"
Run_Commands "pip3 install --no-cache-dir sphinxcontrib-apidoc"



# move to same dir that readthedocs.io uses
Run_Commands "cd $CI_PROJECT_DIR/docs"

#if not exist, create it
Run_Commands "mkdir -p $CI_PROJECT_DIR/docs/_static"


# Commands from readthedocs.io build log
Run_Commands "python3.7 -mvirtualenv $CI_PROJECT_DIR"
Run_Commands "$CI_PROJECT_DIR/bin/python -m pip install --upgrade --no-cache-dir pip"


# build server installs the following as of date: 06 Jun 20
Run_Commands "$CI_PROJECT_DIR/bin/python -m pip install --upgrade --no-cache-dir -r $CI_PROJECT_DIR/test/requirements-docs-buildserver.txt"

# Module requirements
Run_Commands "$CI_PROJECT_DIR/bin/python -m pip install --exists-action=w --no-cache-dir -r $CI_PROJECT_DIR/requirements.txt"

#Doc Requirements
Run_Commands "$CI_PROJECT_DIR/bin/python -m pip install --exists-action=w --no-cache-dir -r $CI_PROJECT_DIR/docs/requirements.txt"

#Conduct the html build
Run_Commands "$CI_PROJECT_DIR/bin/python $CI_PROJECT_DIR/bin/sphinx-build -W -T -b html -d _build/doctrees-readthedocs -D language=en . _build/html"


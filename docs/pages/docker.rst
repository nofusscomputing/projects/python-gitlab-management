Docker
=======
The module has been built into a docker container that can be found on `docker hub <https://hub.docker.com/r/nofusscomputing/gitlab-management>`_. by design the container does not need a command to be specified at runtime, as the ``ENTRYPOINT`` has been set to ``gitlab-management``. Therefore you only need to specify the `arguments <../pages/cli.html>`_ for ``gitlab-management``. If you fail to specify any arguments in the ``docker run`` command, gitlab-management help will be displayed

Building
--------
**To Build, execute the following:**

.. code-block:: bash

  python3 setup.py sdist bdist_wheel
  docker build . --no-cache -t nofusscomputing/gitlab-management:$(cat dist/version)-alpine


Running
-------
**To run the docker image, execute to following:**

.. code-block:: bash

  WORKING_DIR=/Config && docker run -v $PWD:$WORKING_DIR -w $WORKING_DIR nofusscomputing/gitlab-management:$(cat dist/version)-alpine {Arguments}

.. tip:: **Note:** substitute ``{Arguments}`` with the switches for gitlab-management, i.e. ``-h`` for help.
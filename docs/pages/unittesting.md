# Unit Testing
Unit testing is done on all classes and methods. Unit testing has been broken down into the following categories
1. Unit
1. Function
1. Integration

Depending on what item is being tested will depend on where the testing code will go.

## Unit
The Unit test is the most basic of all of the tests. It's intent is to check the following items:
1. Method exists `{Classname}.globals`
1. all Inputs
1. The return values
1. Exceptions
   * Unexpected
   * Expected exceptions are thrown

whilst unit testing, all other methods or classes that the tested method is calling are to be mocked.

## Function
Function testing is intended to test interoperability between classes and methods, including any user input(s). Function testing is to be limited to required dependancies.

The only items that are to be mocked dueing function testing are any methods that require interaction outside of the tested class methods, including dependant modules. *i.e. a http request.*

## Integration
Integration testing is designed to confirm the classes, dependent modules and any required external services work as they were designed. If all tests upto and including this stage of testing pass, it is assumed that the module is ready for release.
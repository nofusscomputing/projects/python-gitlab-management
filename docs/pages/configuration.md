# Configuration
The configuration file for gitlab-management is called `config.yml`. 


## Config.yml
the config file type is yaml. for `gitlab-management` to process, it must be valid yaml.


*Example config file layout*
``` yaml
Group:
    Labels:
        -
            Group: "ExampleGroup1"
            Name: "Stage::Planning"
            Description: "example description for the label"
            Color: "#FF0000"

        -
            Group: 
                - "ExampleGroup1"
                - "ExampleGroup2"
            Name: "Stage::RequireFeedback"
            Description: "example description for the label"
            Color: "#FF0000"
```

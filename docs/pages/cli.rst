Command Line Interface (CLI)
============================
``gitlab-management`` can be run from the command line. to enable functionality when running the command, use arguments. ``-h`` will display help.


``-h`` displays the following

.. code-block:: bash

  GitLab-Management Help
  To run this module the following options are available. There is a short and long version of each option.

  Example: python3 gitlab-management -T {GitlabAuthToken}

  -H    --Host      (Optional) GitLab host to connect to. include http(s)://. Default: https://gitlab.com
  -T    --Token     (Mandatory) GitLab Private token for authentication.
  -l    --labels    Process configuration group.labels.

  -v    --verbose   (Optional) Verbose command output.

  -h    --Help      used to display this help text.


When running the module from the cli, exit codes are returned to denote what has occured. exit code ``0`` is always returned on success.

Please see `cli class <../module/gitlab_management.cli.html>`_ for more details.


See Also
--------
 - `cli Class <../module/gitlab_management.cli.html>`_
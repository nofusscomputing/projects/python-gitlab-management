# Labels

Labels can be managed via gitlab-management python module.


Within the config file [config.yml](configuration.html#Config.yml), the labels are an array (`dict` in python) under path `Group.Labels`. The layout file must be a valid yaml.

>**Tip:** *Labels are cached to keep GitLab API queries as low as possible. A groups labels will only be requested from the GitLab API, `once!!`*

*Example config file layout for labels*
``` yaml
Group:
    Labels:
        -
            Group: "ExampleGroup1"
            Name: "Stage::Planning"
            Description: "example description for the label"
            Color: "#FF0000"

        -
            Group: 
                - "ExampleGroup1"
                - "ExampleGroup2"
            Name: "Stage::RequireFeedback"
            Description: "example description for the label"
            Color: "#FF0000"
```

### Keys

* **Group.Labels.#.Group**: can be a `string` for a single group or `array` of string if you wan't more than one group to have the label.   
* **Group.Labels.#.Name**: The name that you want the label to be.  
* **Group.Labels.#.Description**: The description you want attached to the label.  
* **Group.Labels.#.Color**: a hex representation of the color. in html color notation.  


In the above example, `ExampleGroup1` would get both labels (`Stage::Planning` and `Stage::RequireFeedback`), whereas `ExampleGroup2` will only get label `Stage::RequireFeedback`


### See Also
 - [gitlab-management.ProcessConfigLabels() method](../module/gitlab_management.base.html#gitlab_management.base.GitlabManagement.ProcessConfigLabels)

 - [gitlab-management.GetLabelByName() method](../module/gitlab_management.base.html#gitlab_management.base.GitlabManagement.GetLabelByName)

 - [**Feature** - Labels now cached (issue #8)](https://gitlab.com/nofusscomputing/projects/python-gitlab-management/-/issues/8)


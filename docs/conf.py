# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
from recommonmark.parser import CommonMarkParser

sys.path.insert(0, os.path.abspath('..'))

# -- Project information -----------------------------------------------------

project = 'python GitLab Management'
copyright = '2020, No Fuss Computing'
author = 'No Fuss Computing'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['sphinx.ext.autodoc',
    'sphinx.ext.napoleon',
    'sphinx.ext.linkcode',
    'sphinx.ext.todo',
    'recommonmark',
]


source_suffix = ['.rst', '.md']

autodoc_default_options = {
    'member-order': 'bysource',
    'show-inheritance': False,
    'private-members': True,
    'special-members': 'False',
    'inherited-members': 'False',
    'exclude-members': 'Enum',
    'autodoc_warningiserror': True,
}


napoleon_use_ivar = True
napoleon_use_param = True
napoleon_google_docstring = False

todo_include_todos = True

def linkcode_resolve(domain, info):
    if domain != 'py':
        return None
    if not info['module']:
        return None
    filename = info['module'].replace('.', '/')
    return "https://gitlab.com/nofusscomputing/projects/python-gitlab-management/-/tree/master/%s.py" % filename


# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
#html_theme = 'bizstyle'
html_theme = 'sphinx_rtd_theme'

# ref https://pythonhosted.org/sphinxjp.themes.basicstrap/
#html_theme = 'basicstrap'
#html_theme_options = {
#    'header_inverse': False,
#    'relbar_inverse': True,
#    'inner_theme': True,
#    'inner_theme_name': 'bootswatch-cerulean',
#}


# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# Custom pre-build commands

import subprocess

Commands = [
    ['echo CurrentDirectory $PWD', 'display current working directory'],
    ['pip3 install -r ../requirements.txt', 'setup ready for buildinit to run'],
    ['python3 ../buildinit.py', 'generate the dynamic files, dockerfile and __init__.py'],
    ['mkdir -p includes', 'create temp directory source/includes'],
    ['m2r --dry-run ../README.md > includes/ReadMe.rst', 'convert repo readme to .rst'],
    ['m2r --dry-run ../CONTRIBUTING.md > includes/Contributing.rst', 'convert repo contrib to .rst'],
    ['sphinx-apidoc -f -e -o module ../gitlab_management', 'autodoc the module'],
    ['ls -la module/', 'list the discovered modules'],
]
for Command, Description in Commands:
    print('Running Custom Command: [' + Command + '] ' + Description)
    
    if subprocess.call([Command], shell=True) == 0:
        print("Success")
    else:
        #print("\tFailure")
        sys.exit("Failure running command")
    
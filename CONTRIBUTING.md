# Contribution Guide


## Developer setup
our development workflow is designed to be easy to setup. For this to occur, the following requirements are needed on the developers machine:

- Docker
- sudo access or be part of the group that can execute `docker`
- text editor *(with new lines set to `\n` not `\r\n` as you would see in windows)*


## Module

Setup the environment to use the local module being developed. 

>**Note:** all commands run from the root of the repository directory.


Commands to setup build environment:
``` bash
pip3 install virtualenv

python3 -mvirtualenv $PWD

bin/python -m pip install --upgrade --no-cache-dir pip

bin/python -m pip install --upgrade --no-cache-dir -r requirements.txt

bin/python setup.py develop install

python3 ./buildinit.py
```

test the module being developed
``` bash
bin/gitlab_management -h
```

Clean-up the environment
``` bash
rm -Rf build bin docs/_build gitlab_management.egg-info lib include pyvenv.cfg
```

to build the module
``` bash
python3 setup.py sdist bdist_wheel
```

>**Note:** you must increment the version in the build script (`buildinit.py`) prior to committing your final changes to the repo.

## running builds
Prior to committing to the repo, test builds need to be conducted. we have designed this to replicate gitlabs CI. Each stage of the `.gitlab-ci.yml` can be run from the command line using the following docker command


``` bash
docker run --privileged -w $PWD -v $PWD:$PWD -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:ubuntu-v13.0.0 exec docker ${Gitlab-CI Stage} -env "CI_COMMIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)" --docker-privileged --docker-volumes '/var/run/docker.sock:/var/run/docker.sock'
```
>**tip:** substitute `{Gitlab-CI Stage}` with one of the available stages below before running this command

Tested and confirmed `.gitlab-ci.yml` tasks as working with the above command
- **Verify**
    * PyLint
- **Unit Testing**
    * Unit Test
    * Function Test
- **package**
    * Coverage *- Not Working*
    * gitlab-management_package
- **build**
    * ~~Docker_Build-Alpine~~ *- not working*
- **test**
    * ~~pages~~ *- Only usable on GitLab*
    * ~~bandit-sast~~ *- Not Tested*
    * ~~Scan gitlab-management-Alpine~~ *- Not Tested*
    * ~~gemnasium-python-dependency_scanning~~ *- Not Tested*
    * ~~license_scanning~~ *- Not Tested*
- **validate**
    * ~~Documentation~~ *- Not Working*   
      **Tip:** use this command to build/test the docs 
      ```bash
      cd {Repo Directory}

      rm -Rf build bin docs/_build gitlab_management.egg-info lib include pyvenv.cfg docs/module docs/includes dist/version gitlab_management/__init__.py

      CI_PROJECT_DIR=/Repository && docker run -e CI_PROJECT_DIR=$CI_PROJECT_DIR -w $CI_PROJECT_DIR -v $PWD:$CI_PROJECT_DIR readthedocs/build:latest bash test/validation-build-docs.sh
      ```
- **release**
    * ~~GitLab-Release~~ *- Only usable on GitLab*
- **publish**
    * ~~Publish~~ *- Only usable on GitLab*



### Version Changes
Every change, prior to being committed to the `development` branch, must have it's version incremented. In most cases only the `{newfeature}` number will need to be incremented.

if the version is less than `1.0.0` the version number layout is `0.{newfeature}.{bugfix}`

if the version number is more than `1.0.0` the version number layout is `{majorchange}.{newfeature}.{bugfix}`.  
`{majorchange}` should only be incremented if the end user would be required to make a manual adjustments to use the version.

To adjust the version, open `buildinit.py` and edit the `__version__` property at the top of the script.

Regardless of the version until the changes are pushed to the `development` branch, it will be adjusted to be a release candidate. the appended version information will be `.rc` with a formated date i.e. `.rcYYMMDDHHMMSS`.

## Documentation
We use Sphinx for ducumentation. All documentation for the project lives in the `docs/` directory within the repo. Documentation can be written in either RestructureText `.rst` or MarkDown `.md` files. Documentation for the modules however, is written within the code using `NumPy` style `DocBook` code comments. These comments are then rendered to `.rst` via the `sphinx.ext.autodoc` extension.

All Documentation is to be written in english. Since it depends on what dialect of english you speak, spelling can be different. The documentation spelling for plain speak (i.e. a sentance of text) will be Australian english. 

Any additional pages are to be written in `.md` and placed in the directory `docs/pages`.


Further examples can be viewed [here](https://www.sphinx-doc.org/en/master/usage/extensions/example_numpy.html#example-numpy).


To document a property/variable add a single line comment underneath i.e.            
``` python
DemoProperty:str = 'Property Value'
"""A single line describing the purpose of the property"""
```

### template class documentation

The following is the template for a class. *(delete sections that are not required)*

``` python
class ExampleClassToDocument:
"""The summary line for a class docstring should fit on one line.

If the class has public attributes, they are not to be documented here. any parameters the class has as part of it's ``__init__`` should be documented in this section.

Example
-------
It should be assumed that a person using this class would have a working knowldege of python. However, if there are any intricacies that are outside of a simple string, this section should be used to explain how to use. 

Parameters
----------
MandatoryParameter : str
    [Single Line summary]

OptionalParameter : str, optional
    [Single Line summary]

Attention
---------
Don't add an Attributes section to a class.

See Also
--------
`Dependent Methods`
    List methods alphabetically here one per line and as a hyperlink to the method.
    `Output <#gitlab_management.base.GitlabManagement.Output>`_

"""
```

### template method documentation
The following is the template for a method. *(delete sections that are not required)*
``` python
def ExampleMethod(MandatoryParameter:str, OptionalParameter:str=None):
"""[one line summary]

[detailed summary formated as paragraphs]

Example
-------
Examples can be given using either the ``Example`` or ``Examples``
sections. Sections support any reStructuredText formatting, including
literal blocks::

    $ python example_numpy.py


Section breaks are created with two blank lines. Section breaks are also
implicitly created anytime a new section starts. Section bodies *may* be
indented:


Parameters
----------
MandatoryParameter : str
    [Single Line summary]

OptionalParameter : str, optional
    [Single Line summary]

    NOTE
    ----
    Notes and other tip boxes can be included in any section, just don't forget to indent.

Returns
-------
bool
    True if successful, False otherwise.

See Also
--------
`Dependent Methods`
    List methods alphabetically here one per line and as a hyperlink to the method.
    `Output <#gitlab_management.base.GitlabManagement.Output>`_

"""
```

### Building
Buiding of the docs is part of the *validation* stage of the build pipeline. The doc build, must complete without any warnings to be considered a pass.

Please build the docs locally before pushing to the repo, this will ensure that they are working befor being committed. After a local build the docs can be found in the repository directory under `docs/_build/html/index.html`


Clean-up the environment prior to building the documentation, with:
``` bash
rm -Rf build bin docs/_build gitlab_management.egg-info lib include pyvenv.cfg
```



>**Tip:**
The above command contains the setting of the environmental variable `CI_PROJECT_DIR`, this is the directory where the repository is to be mounted to within the container. *don't include a trailing `/` slash if you change this.* The variable `$PWD` is to mount the current directory, if you are not running this command from the repo root dir, please set this to the full path of the repo.

